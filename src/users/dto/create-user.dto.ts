import { IsNotEmpty, Length } from 'class-validator';
export class CreateUserDto {
  @IsNotEmpty()
  @Length(4, 32)
  fullName: string;

  @IsNotEmpty()
  @Length(10)
  tel: string;
}
