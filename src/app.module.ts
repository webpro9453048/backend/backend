import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { HistoryService } from './history/history.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DataSource } from 'typeorm';
import { HistoryEntity } from './history/history.entity';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'sqlite',
      database: 'd-coffeedb.sqlite',
      entities: [HistoryEntity],
      synchronize: true,
    }),
    TypeOrmModule.forFeature([HistoryEntity]),
  ],
  controllers: [AppController],
  providers: [HistoryService],
  exports: [],
})
export class AppModule {
  constructor(private dataSource: DataSource) {}
}
